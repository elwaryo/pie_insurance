# Problem
Count the words in the given text and print return a dictionary that
has the most popular words first.

The code in [problem_1_start.py](./problem_1_start.py) is the basis
for the implementation. Fill in the code in the `_count_and_print_words`
function to implement the word counting and ranking functionality. 